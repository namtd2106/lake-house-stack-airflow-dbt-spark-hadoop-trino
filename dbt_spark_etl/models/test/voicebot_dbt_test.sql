{{ config(
    materialized='incremental',
    file_format='delta',
    location_root='/warehouse',
    pre_hook={
        "sql": "CREATE TEMPORARY VIEW voicebot_loans
USING org.apache.spark.sql.jdbc
OPTIONS (
  url 'jdbc:oracle:thin:@//{{ env_var('DWH_HOST') }}:1521/orcldwh1',
  user '{{ env_var('DWH_USER') }}',
  password '{{ env_var('DWH_PASS') }}',
  query 'with data as (select DATE_WID, INTEGRATION_ID, FIRST_NAME, LAST_NAME
from F88DWH.W_VOICEBOT_LOANS
where DATE_WID = 20240402 AND INTEGRATION_ID IN (\\'660b3db76558720c1bdfb479\\', \\'660b3db76558720c1bdfb0da\\'))
select DATE_WID, INTEGRATION_ID, FIRST_NAME, LAST_NAME from data
'
);
",
    "transaction": True
}
)}}

select DATE_WID, INTEGRATION_ID, FIRST_NAME, LAST_NAME
from voicebot_loans