#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL
  DROP DATABASE IF EXISTS hive_db;
  DROP ROLE IF EXISTS hive;
  CREATE USER hive WITH PASSWORD 'hive';
  CREATE DATABASE hive_db;
  GRANT ALL PRIVILEGES ON DATABASE hive_db TO hive;
  \c hive_db
  \t
  \o /tmp/grant-privs
SELECT 'GRANT SELECT,INSERT,UPDATE,DELETE ON "' || schemaname || '"."' || tablename || '" TO hive ;'
FROM pg_tables
WHERE tableowner = CURRENT_USER and schemaname = 'public';
  \o
  \i /tmp/grant-privs
EOSQL